const getKeycloakClient = async () => {
    const keycloakAdmin = (await import('@keycloak/keycloak-admin-client')).default;
    const client = new keycloakAdmin({
        baseUrl: process.env.KEYCLOAK_URL,
        realmName: 'FederatedCatalog',
    });

    await client.auth({
        username: process.env.KEYCLOAK_USERNAME,
        password: process.env.KEYCLOAK_PASSWORD,
        clientId: 'FederatedCatalog',
        grantType: 'password'
    });
    
    return client;
}


const getUserKeycloakId = async (email) => {
    try {
        const keycloakClient = await getKeycloakClient();
        const users = await keycloakClient.users.find({
            email: email,
        });
        return { status: users.length == 1, id: users[0]?.id };
    } catch (error) {
        console.log(error)
        throw new Error(`Error to get keycloak user id for user ${email}: ${error}`);
    }
}

const getUserInfo = async (userId) => {
    try {
        const keycloakClient = await getKeycloakClient();
        const userDetails = await keycloakClient.users.findOne({
            id: userId
        });
        return {
            "gx-signature:userid": userDetails.id,
            "gx-signature:email": userDetails.email,
            "gx-signature:did": userDetails.username,
            "gx-signature:firstname": userDetails.firstName,
            "gx-signature:lastname": userDetails.lastName,
            "gx-signature:mobile": "0646273476"
        };
    } catch (error) {
        console.log(error)
        throw new Error(`Error to get keycloak user id for user ${email}: ${error}`);
    }
}

module.exports = {
    getUserInfo,
    getUserKeycloakId
}