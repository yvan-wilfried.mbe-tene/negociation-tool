const { createLogger, format, transports } = require("winston");

const logger = createLogger({
    format: format.combine(
        format.timestamp(),
        format.json()
    ),
    transports: [new transports.Console({})],
});

// Personnalisez le format du message de journal en déplaçant le timestamp au début
logger.format = format.combine(
    format.timestamp(),
    format.printf(({ timestamp, level, message, ...rest }) => {
        return JSON.stringify({ timestamp, level, message, ...rest });
    })
);

module.exports = logger;