const logger = require("./logger");
const Mailjet = require('node-mailjet');
const negoNotificationTemplate = require('../models/email/negoNotification.json');

const mailjet = Mailjet.apiConnect(
    '36872c127ba3ef46aa69cdcff4265cf4',
    '878ba0cdeba727f85ad00b3296056861',
);


function sendMail(data) {
    const request = mailjet
        .post('send', { version: 'v3.1' })
        .request({
            Messages: [
                {
                    From: {
                        Email: process.env.MAIL_SENDER,
                        Name: process.env.NAME_SENDER
                    },
                    To: [
                        {
                           // Email: 'yvan-wilfried.mbe-tene@softeam.fr',
                            Email: data.email,
                            Name: data.firstname
                        }
                    ],
                    Subject: data.subject,
                    HTMLPart: data.HTMLPart
                }
            ]
        })

    request.then(result => {
        logger.info(`Email send at ${data.email}`);
    }).catch(err => {
        console.log(err.statusCode);
    });
}

function replaceTemplateVariables(template, variables) {
    let templateWithValues = template;
    for (const key in variables) {
      templateWithValues = templateWithValues.replace(new RegExp(`{{${key}}}`, 'g'), variables[key]);
    }
    return templateWithValues;
}


function sendNegoNotificationMail(data) {
    const mailBody = replaceTemplateVariables(negoNotificationTemplate.Message, data);
    const mailData = {
        email: data.email,
        firstname: data.firstName,
        subject: negoNotificationTemplate.Subject,
        HTMLPart: mailBody
    }
    sendMail(mailData);
}

module.exports = {
    sendNegoNotificationMail    
}