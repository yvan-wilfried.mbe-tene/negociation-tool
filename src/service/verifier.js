const axios = require("axios");

async function verifyCredential(credential) {
    try {
        const response = await axios.post(process.env.VERIFIER_URL, [credential]);

        if (response.status !== 200) {
            throw new Error("The verifiable credential is not valid");
        }
    }
    catch (error) {
        throw new Error("The verifiable credential is not valid");
    }
}

module.exports = {
    verifyCredential
}