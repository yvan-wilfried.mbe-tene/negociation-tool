FROM node:alpine

WORKDIR /usr/src/app

ENV PATH usr/src/app/node_modules/.bin:$PATH

COPY ./src ./src
COPY .env .

COPY package*.json .

RUN npm install --silent

RUN apk add --no-cache \
    udev \
    ttf-freefont \
    chromium \
    harfbuzz \
    nss

EXPOSE 3002
CMD ["npm", "start"]
